#!/bin/sh
# install script for rbwmenu
# mintezpresso

# EDIT THIS FUNCTION TO CHANGE DEFAULT INSTALL COLOR
fn_color () {
# prompt_bg and desc_bg uses bitwarden logo color

printf "asterisk= \"# \";
prompt = \"\$\";
font = \"Droid:pixelsize=12\";
prompt_fg = \"#fbfbfd\";
prompt_bg = \"#175ddc\";
normal_fg = \"#fbfbfd\";
normal_bg = \"#1e1d2d\";
select_fg = \"#eeeeee\";
select_bg = \"#175ddc\";
desc_fg = \"#eeeeee\";
desc_bg = \"#175ddc\";
" > $HOME/.gnupg/pinentry-dmenu.conf
}

# This is where rbwmenu will be installed to
# Change this to your preferred location, or chose from one of the presets

destdir="/usr/local/bin"
#destdir="$HOME/.local/bin"
#destdir="$HOME/.local/bin/menu" # mintezpresso's dir. this translates to ~/.local/bin/menu/rbwmenu

# No need to touch anything below this line, unless you know exactly what you're doing

fn_lack_dep () {
    printf "One or more of these core dependencies not found:
- dmenu
- rbw
- dunst
- xclip
- GNU sed (for install script only)

# Optional

- xdotool: click prompt noti from keyboard
- clipmenu: reaccess recent entry without needing to call rbwmenu again

Troubleshoot hint:
- Check if your \$PATH is pointing to basic Linux \$PATH like /usr/bin, /usr/local/bin, /usr/sbin.
- Install all dependencies. Package name might be different per distro
"
    exit
}

fn_install () {
    printf "This install script ships the author's pinentry-dmenu theme.\nTo change color later, edit \$HOME/.gnupg/pinentry-dmenu.conf
To change color now, edit this install script then run it again
Stop the installation and change color (y/N): "
    read yn

    [ "$yn" = "y" ] && printf "User canceled rbwmenu installation\nInstall script editing hint: It's at the top\n" && exit

    [ ! -f "$HOME/.config/rbw/config.json" ] && dunstify -t 15000 "rbw not configured, or config file is broken" "\nRun \"rbw config set email your_bit_warden_login_email\" to config.\n\nMake sure $HOME/.config/rbw/config.json exists\n\nOnce that's done, start rbwmenu again" && exit 1

    # Change rbw config in ~/.config/rbw/config.json from pinentry to pinentry-dmenu
    sed -i 's/\"pinentry\"\}/\"pinentry-dmenu\"\}/g' $HOME/.config/rbw/config.json

    # Create config file
    touch $HOME/.config/rbw
    cp menu.conf $HOME/.config/rbw/menu.conf
    sudo chmod +x $HOME/.config/rbw/menu.conf

    # Change current user's pinentry program to pinentry-dmenu
    mkdir -p $HOME/.gnupg
    touch $HOME/.gnupg/gpg-agent.conf
    printf "pinentry-program $(command -v pinentry-dmenu)" >> $HOME/.gnupg/gpg-agent.conf

    [ ! -e "$HOME/.gnupg/pinentry-dmenu.conf" ] && fn_color
    # now we create folor profile for the thing, if it hasnt exist yet

    # Create pinentry-dmenu configs
    touch $HOME/.gnupg/pinentry-dmenu.conf


    [ ! "$(command -v rbwmenu)" ] && sudo cp rbwmenu $destdir/rbwmenu && sudo chmod +x $destdir/rbwmenu
    printf "Done\n"
}

# - install:
#     - if already exist rbwmenu then just overwrite it
#     - ask user about install dir. highlight default
#     - learn to overwrite user configs with new shits too

fn_update () {
    # THIS ASSUMES USER ALREADY HAS RBWMENU INSTALLED AND CONFIGURED
    # INCOMPLETE
    case $destdir in
        *usr* ) sudo cp rbwmenu $destdir/rbwmenu && sudo chmod +x $destdir/rbwmenu ;;
        * ) cp rbwmenu $destdir/rbwmenu && chmod +x $destdir/rbwmenu ;;
    esac

    cp menu.conf $HOME/.config/rbw/menu.conf.new

    printf "New config files installed as \"\$HOME/.config/rbw/menu.conf.new\"\nPlease update your configs accordingly\n"
}

[ -n "$(command -v pinentry-dmenu)" ] && [ -n "$(command -v rbw)" ] && [ -n "$(command -v dmenu)" ] && [ -n "$(command -v xclip)" ] && [ -n "$(command -v dunst)" ] && fn_install || fn_lack_dep
