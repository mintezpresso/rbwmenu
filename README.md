## Simple [dmenu](https://tools.suckless.org/dmenu/) wrapper to interact with some functions of [rbw](https://github.com/doy/rbw)

Tested on Artix OpenRC + Void, and is designed to run from keyboard shortcut

## Install
- Step 1: Install dependencies

All dependencies are available on the AUR and Void repo

- Must have:
    - dmenu: tested on fresh and riced suckless downloaded build
    - [rbw](https://github.com/doy/rbw): get latest version. [rbw-git](https://aur.archlinux.org/packages/rbw-git) is on AUR
    - dunst
    - xclip
    - pinentry-dmenu: [master](https://github.com/ritze/pinentry-dmenu) or [backup](https://codeberg.org/mintezpresso/pinentry-dmenu)
    - GNU sed: for install script. You can skip this if you want to install rbwmenu manually
- Optional:
    - xdotool: click prompt noti from keyboard
    - [clipmenu](https://github.com/cdown/clipmenu): reaccess recent entry without needing to run rbwmenu again

- Step 2: Get code
    - ``git clone https://codeberg.org/mintezpresso/rbwmenu``

- Step 3: Edit your options (Optional)
    - ``cd rbwmenu``, then edit ``install.sh`` to your preferences. Instructions are fairly newb friendly

- Step 4: ``./install.sh``
